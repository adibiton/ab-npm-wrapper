const npm = require('npm')
const {promisify} = require('util')

const load = promisify(npm.load)
module.exports = {
    getPrefix: async () => {
        try{
            await load()
            return npm.config.get('prefix')
        } catch(e){
            throw new Error('Error while loading npm with: ', err)
        }
    },
    getGlobalDir: async () => {
        try {
            await load()
            return npm.globalDir
        } catch(e){
            throw new Error('Error while loading npm with: ', err)
        }
    }
}

