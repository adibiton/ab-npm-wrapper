### ab-npm-wrapper
simplify npm api

#### How to call it
``` js
const npm = require('ab-npm-wrapper')
npm.getPrefix().then(prefix => console.log(prefix))

npm.getGlobalDir().then(dir => console.log(dir))
```